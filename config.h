#ifndef	__CONFIG_H__
#define	__CONFIG_H__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>

typedef		unsigned char	uint8;
typedef		unsigned int	uint16;
typedef		unsigned long	uint32;

#include "myfile.h"
#include "crc16.h"

#endif
