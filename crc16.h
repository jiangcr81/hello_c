#ifndef  __CRC16_H__
#define  __CRC16_H__

unsigned int calcCrc16(unsigned char *data, int len, int preval);

#endif
