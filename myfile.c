#include "config.h"

int get_file_size(char * filename)
{
	int ret = 0;
	FILE* file = fopen(filename, "r");
	if(file)
	{
		fseek(file, 0, SEEK_END);	//move to the end of file
		ret = (int)ftell(file);		//get the offset by the start of file
		fclose(file);
	}
	return ret;
}

int read_file_content0(char * filename)
{
#define TMP_BUF_SIZE	1024
	int ret = 0;
	unsigned char in[TMP_BUF_SIZE];
	FILE* file = fopen(filename, "r");
	if (file)
	{
		while( !feof(file))
		{
			memset(in, 0, sizeof(in));
			ret = fread(in, sizeof(char), TMP_BUF_SIZE, file);
			if(ferror(file))
			{
				printf("file read error, ret = %d\r\n", ret);
				break;
			}
			for(int i=0; i<ret; i++)
			{
				printf("%.2X ", in[i]);
				if((i>0) && (i%16 == 15))
				{
					printf("\r\n");
				}
			}
		}
		fclose(file);
	}
	return ret;
}

int read_file_content(char * filename, char * content)
{
#define TMP_BUF_SIZE	1024
	int ret = 0;
    int offset = 0;
	unsigned char in[TMP_BUF_SIZE];
	FILE* file = fopen(filename, "r");
	if (file)
	{
		while( !feof(file))
		{
			memset(in, 0, sizeof(in));
			ret = fread(in, sizeof(char), TMP_BUF_SIZE, file);
			if(ferror(file))
			{
				printf("file read error, ret = %d\r\n", ret);
				break;
			}
			for(int i=0; i<ret; i++)
			{
				//printf("%.2X ", in[i]);
				if((i>0) && (i%16 == 15))
				{
				//	printf("\r\n");
				}
			}
            memcpy(&content[offset], in, ret);
            offset += ret;
		}
		fclose(file);
	}
	return ret;
}

void gen_dat(int crcsum)
{
    unsigned char buf[16];
    FILE * outfile = NULL;
    char outfilename[] = "cst92f30.dat";
	buf[0] = 0xFF;
	buf[1] = 0xFF;
	buf[2] = 0xFF;
	buf[3] = 0xFF;
	buf[4] = 0xFF;
	buf[5] = 0xFF;
	buf[6] = 0xFF;
	buf[7] = 0xFF;
	buf[8] = 0x01;
	buf[9] = 0x00;
	buf[10] = 0xFE;
	buf[11] = 0xFF;
    buf[12] = crcsum&0xFF;
    buf[13] = (crcsum>>8)&0xFF;
    buf[14] = 0;
    buf[15] = 0;

    outfile = fopen(outfilename, "w+");
    fwrite(buf, sizeof(unsigned char), 14, outfile);
    fclose(outfile);
}

void gen_json(int crcsum)
{
        FILE * outfile = NULL;
        char outfilename[] = "manifest.json";
        char top[] = "\
{\r\n\
    \"manifest\": {\r\n\
        \"application\": {\r\n\
            \"bin_file\": \"cst92f30.bin\",\r\n\
            \"dat_file\": \"cst92f30.dat\",\r\n\
            \"init_packet_data\": {\r\n\
                \"application_version\": 4294967295,\r\n\
                \"device_revision\": 65535,\r\n\
                \"device_type\": 65535,\r\n\0";
    char bottom[] = "\
                \"softdevice_req\": [\r\n\
                    65534\r\n\
                ]\r\n\
            }\r\n\
        },\r\n\
        \"dfu_version\": 0.5\r\n\
    }\r\n}\0";
        char crch[] = "                \"firmware_crc16\":\0";
        char crc[10] = {0x20};
        sprintf(crc, "%d,\r\n", crcsum);
        printf("%s", top);
        printf("%s", crch);
        printf("%s", crc);
        printf("%s", bottom);
        printf("\r\n");
        //printf("size of crc=%d\r\n", sizeof(crc));
        outfile = fopen(outfilename, "w+");
        fwrite(top, sizeof(char), sizeof(top)-2, outfile);
        fwrite(crch, sizeof(char), sizeof(crch)-2, outfile);
        if(crcsum<10000) fwrite(crc, sizeof(char), sizeof(crc)-3, outfile);
        else fwrite(crc, sizeof(char), sizeof(crc)-2, outfile);
        fwrite(bottom, sizeof(char), sizeof(bottom)-2, outfile);
        fclose(outfile);
}

