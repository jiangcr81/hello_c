
#COMPILE    = mipsel-linux-
COMPILE     =   
CC          = $(COMPILE)gcc
AR          = $(COMPILE)ar
LD          = $(COMPILE)ld
OBJCOPY     = $(COMPILE)objcopy
NM      = $(COMPILE)nm
OBJDUMP     = $(COMPILE)objdump
CHMOD       = chmod

PWD = $(shell pwd)

BIN	= crc4ota
SRCS	= $(wildcard *.c)  
SRCS   += $(wildcard util/*.c) 

CFLAGS	= -ggdb -Wall 
CFLAGS += -I.
#CFLAGS += -I$(PWD)/util
LDFLAGS = 
LIBFLAGS = 

OBJS += $(addsuffix .o, $(basename $(SRCS)))

all: $(BIN)

$(OBJS): %.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@

$(BIN): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $^ $(LIBFLAGS)

clean:
	$(RM) $(BIN) *.o *.dat *.json *.zip $(OBJS)

zip:
	$(RM) *.o *.dat *.json *.zip $(OBJS)
	./$(BIN)
	7z a cst92f30ota.zip cst92f30.bin cst92f30.dat manifest.json
	cp cst92f30ota.zip ~/sd0/Download/
