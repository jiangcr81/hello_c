#ifndef __MYFILE_H__
#define __MYFILE_H__

int get_file_size(char * filename);

int read_file_content0(char * filename);
int read_file_content(char * filename, char * content);
void gen_dat(int crcsum);
void gen_json(int crcsum);

#endif
