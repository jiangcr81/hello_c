#include "config.h"

int main(int argc, char * argv[])
{
    char * fc;
	int binfilesize = 0;
    unsigned int crcsum = 0;
	printf("crc for ota V1.0  Powered by Wilson @ 2021\r\n");
	printf("Usage: crc4ota [bin file] <= default bin file is 'cst92f30.bin'\r\n");
	if(argc == 1)
	{
		binfilesize = get_file_size("cst92f30.bin");
		printf("bin file size = %d", binfilesize);
		printf("\r\n");
        if(binfilesize > 0) {
            fc = (char *)malloc(binfilesize);

		    read_file_content("cst92f30.bin", fc);
            crcsum = calcCrc16((unsigned char *)fc, binfilesize, 0xffff);
	        printf("crcsum= 10:%d, 0x%X\r\n", crcsum, crcsum);
        }
	}
	else if(argc == 2)
	{
		binfilesize = get_file_size(argv[1]);
		printf("bin file size = %d", binfilesize);
		printf("\r\n");
        if(binfilesize > 0) {
            fc = (char *)malloc(binfilesize);
		
            read_file_content(argv[1], fc);
            crcsum = calcCrc16((unsigned char *)fc, binfilesize, 0xffff);
	        printf("crcsum = 10:%d, 0x%X\r\n", crcsum, crcsum);
        }
	}
    gen_dat(crcsum);
    gen_json(crcsum);
	return 0;
}

